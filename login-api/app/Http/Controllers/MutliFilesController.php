<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MultiFile;
use Validator;

class MutliFilesController extends Controller
{
    //Show file Upload form
    public function showForm(){
        return view('multiple-file-upload.fileUpload');
    }

    //Save Files In database
    public function sendFiles(Request $request){
        $this->validate($request, [
            'images' => 'required|mimes:jpg,jpeg,png',
            'images.*' => 'required'
    ]);
         
            try{
                $images=$request->file('images');
        
                $files = [];
                if($request->hasfile('images'))
                 {
                    foreach($images as $file)
                    {
                        $name = $file->getClientOriginalName();
                        $file->move(public_path('/assets/files'), $name);  
                        $files[] = $name;  
                    }
                 }
                 $images=implode(",",$files);
                 $file= new MultiFile();
                 $file->images = $images;
                 $file->save();
          
                return back()->with('success', 'Data Your files has been successfully added');
            }catch(\Exception $ex){
                return back()->with('failed',$ex->getMessage());
            }
         
      
    }


}
