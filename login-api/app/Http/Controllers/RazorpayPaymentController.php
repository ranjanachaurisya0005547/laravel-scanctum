<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use Razorpay\Api\Api;
use Session;

class RazorpayPaymentController extends Controller
{
    //payment form
    public function showPaymentForm(){
        return view('paymentGateway.paymentView');
    }

    //payment store
    public function makePayment(Request $request){
        $name=$request->input('name');
        $amount=$request->input('amount');

        $api_key=env('RAZOREPAY_KEY');
        $api_secret=env('RAZORPAY_SECRET');

        $api=new Api($api_key,$api_secret);
        $order  = $api->order->create(array('receipt' => '123', 'amount' => $amount * 100, 'currency' => 'INR')); 
        $orderId=$order['id'];

         $product=Payment::create([
               'name'=>$name,
               'amount'=>$amount,
               'payment_id'=>$orderId,
         ]);

         $product->save();

        Session::put('order_id',$orderId);
        Session::put('amount',$amount);

       return redirect('/razorpay-gateway');
        

    }

    public function paySuccess(Request $request){
        $data=$request->all();
        //dd($data);
        $user=Payment::where('payment_id',$data['razorpay_order_id'])->first();
        $user->payment_done=true;
        $user->razorpay_id=$data['razorpay_payment_id'];
        $user->save();

        return view('paymentGateway.successpayment');
    }
}
