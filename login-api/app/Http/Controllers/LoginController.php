<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class LoginController extends Controller
{
    //User Registration
    public function register(Request $request){
            
            $rules=[
                  'name'=>'required|string|regex:/^[A-Za-z ]+$/',
                  'email'=>'required|email',
                  'password'=>'required',
            ];

            $validate=Validator::make($request->all(),$rules);

            if($validate->fails()){
                 return response()->json(['message'=>$validate->errors(),'success'=>false,'status'=>201]);
            }else{

                try{
                    $data=$request->input();
                     $user=User::create([
                         'name'=>$data['name'],
                         'email'=>$data['email'],
                         'password'=>Hash::make($data['password']),
                     ]);

                     return response()->json(['message'=>"Registration Done Successfully !",'success'=>true,'status'=>200]);
                }catch(\Exception $ex){
                     return response()->json(['message'=>$ex-getMessage(),'success'=>false,'status'=>201]);
                }


            }
    }


    //User Login
    public function login(Request $request){
        $rules=[
           'email'=>'required',
           'password'=>'required',
        ];

        $validate=Validator::make($request->all(),$rules);

        if($validate->fails()){
            return response()->json(['message'=>$validate->errors()]);
        }else{
            try{
                $data=$request->input();

                $user=User::where('email',$data['email'])->first();
                if($user){
                    if(Hash::check($data['password'],$user->password)){

                       $token=$user->createToken("accessToken")->plainTextToken;
                       return response()->json(['message'=>"Login Successfully !",'success'=>true,'status'=>200,'accessToken'=>$token]);
                    }else{
                        return response()->json(['message'=>'Invalid Password !','success'=>false,'status'=>403]);
                    }

                }else{
                    return response()->json(['message'=>'Invalid Email Addess !','success'=>false,'status'=>402]);
                }

            }catch(\Exception $ex){
                return response()->json(['message'=>$ex->getMessage(),'success'=>false,'status'=>401]);
            }
        }
    }


    //Admin Dashboard
    public function adminDashboard(){
        return view('admin.dashboard');
    }

    //User Logout
    public function logout(Request $request){
        $request->user()->currentAccessToken()->delete();

        return response()->json(['message'=>'Logout Successfully !','success'=>true,'status'=>200]);
    }
}
