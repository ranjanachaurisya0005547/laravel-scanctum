<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Multiple File Upload Example...........</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6 mt-5">
			@if(session('success'))
                  <div class="alert alert-success">{{session('success')}}</div>
			@elseif(session('failed'))
                  <div class="alert alert-danger">{{session('failed')}}</div>
			@endif
			<div class="card mt-5">
				<div class="card-header"><b>Multiple File Uploading......</b></div>
				<div class="card-body">
					<form action="{{route('file.save')}}" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="form-group ">
							<label for="file">Chosse Your files</label>
							<input type="file" name="images[]" id="file" class="form-control" multiple/>

							@if($errors->has('images'))
                             <div class="alert alert-danger mt-2">{{$errors->first('images')}}</div>
							@endif

                        </div>
						<input type="submit" name="file-btn" value="Save" class="btn btn-primary btn-lg"/>
					</form>
				</div>
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
</html>