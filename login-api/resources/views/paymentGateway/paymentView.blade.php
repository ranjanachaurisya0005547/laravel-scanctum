<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Laravel - Payment Gateway Using Razorpay...</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

</head>
<body>
<div class="container">

    <!-------------Paymnet Submission Form----------------------->
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="card mt-5">
                <div class="card-header">
                    Payment With Razorpay...
                </div>
                <div class="card-body">
                    <form action="{{route('razorpay.payment.store')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Enter Your Name</label>
                            <input type="text" name="name" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Enter Your Amout</label>
                            <input type="text" name="amount" class="form-control"/>
                        </div>
                      <input type="submit" class="btn btn-primary"/>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-3"></div>
    </div>

    <!------------------Razorpay submission Form-------------->
    @if(Session::has('amount'))
    <div class="row">
        <form action="{{ route('razorpay.success') }}" method="POST" >
                                    @csrf
                                    <script src="https://checkout.razorpay.com/v1/checkout.js"
                                            data-key="{{ env('RAZOREPAY_KEY') }}"
                                            data-amount="{{Session::get('amount')}}"
                                            data-currency="INR"
                                            data-buttontext="Pay with Razorpay"
                                            data-name="pizza"
                                            data-order_id="{{Session::get('order_id')}}"
                                            data-description="Rozerpay"
                                            data-prefill.name="name"
                                            data-prefill.email="email"
                                            data-theme.color="#ff7529"
                                    >
                                    </script>
                                 <input type="hidden" custom="Hidden Element" name="hidden">
                                </form>
    </div>
    @endif
</div>
</body>
</html>