<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\RazorpayPaymentController;
use App\Http\Controllers\MutliFilesController as Files;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//pdf geration with image
Route::get('generate-pdf',[PDFController::class,'generatePDF']);

//barcode generator
Route::view('barcode', 'barcode.barcode');

//QRcode Generation 
Route::get('qr-code-g', function () {
  
    \QrCode::size(500)
            ->format('png')
            ->generate('myqrcode', public_path('assets/images/qrcode.png'));
    
  return view('barcode.qrCode');
    
});

//Payment Gateway Integration
Route::get('/razorpay-gateway',[RazorpayPaymentController::class,'showPaymentForm'])->name('payment.gateway');
Route::post('razorpay-payment',[RazorpayPaymentController::class,'makePayment'])->name('razorpay.payment.store');
Route::post('/payment_success',[RazorpayPaymentController::class,'paySuccess'])->name('razorpay.success');

//Multiple File Uploading 
Route::prefix('/user')->group(function(){
    Route::get('upload-form',[Files::class,'showForm'])->name('file.form');
    Route::post('/save-files',[Files::class,'sendFiles'])->name('file.save');

});